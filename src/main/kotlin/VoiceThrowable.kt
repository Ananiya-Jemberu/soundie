internal sealed class VoiceThrowable : Throwable(){
    object Throw {
        val pythonNotFound: Throwable = throw Throwable("Python is not found")
        val pipIsNotFound: Throwable = throw Throwable("pip is not set")
        val driverIsNotInitialized: Throwable = throw Throwable("the Driver is not initialized yet")
    }
}
