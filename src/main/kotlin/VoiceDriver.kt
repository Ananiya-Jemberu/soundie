import py.PyBuilder
import java.io.File
import java.io.FileWriter
import java.io.PrintWriter

class VoiceDriver: VoiceManager {
    private fun processor() = VoiceProcess().processor()
    private val file = File("${System.getProperty("user.dir")}//build//python_cache//sound.py")
    private val wrd = """# Import
import pyttsx3

# engine
engine = pyttsx3.init()"""
    /**
     * Used to initialize the driver before use
     * */
    fun `init`() {
        if(!file.exists()){
            File("${System.getProperty("user.dir")}//build//python_cache").mkdir()
            file.createNewFile()
        }
        PyBuilder(file).apply{
            import0("pyttsx3")
            body {
                """
# engine
engine = pyttsx3.init()"""
            }
        }
    }

    /** will spoke the word you given.
     *
     * Usage:
     * ```kt
     * VoiceDriver.`init`()
     * VoiceDriver.spoke("the word to be spoken")
     * ```
     *
     * **Note**: must be initialized before use with [VoiceDriver.`init`()]
     *
     * */
    private val setVoice = {type: String -> """voices = engine.setProperty('voices',$type)"""}
    fun spoke(word: String, voice: String? = null) {
        if (wrd in file.readLines().joinToString("\n")) {
            if(voice != null){
                val out = PrintWriter(FileWriter(file, true), true)
                out.append(setVoice(voice))
            }
            PyBuilder(file).apply {
                body {
                    """
engine.say("$word")
engine.runAndWait()                          
                    """
                }
            }
            processor()
        } else VoiceThrowable.Throw.driverIsNotInitialized

    }
    private val getAllVoice = """
voices = engine.getProperty('voices')
print(voices)
    """
    /**
     * fetch all available voices in the machine
     *
     * */
    fun getVoices(): Any{
        var wrd2: Any? = null
        if(wrd in file.readLines().joinToString("\n")){
            PyBuilder(file).apply {
                getAllVoice
            }
        } else VoiceThrowable.Throw.driverIsNotInitialized

        if (processor() != "" &&(processor().first() == '[' && processor().last() == ']' )){
            wrd2 = processor().substring(1,(processor().length - 1)).split(",")
        }else {
            wrd2 = processor()
        }
        return wrd2
    }
    /**
     * this function put the given word in loop
     * */
    fun spokeWithLoop(word: String, repetition: Int){
        repeat(repetition){
            spoke(word)
        }
    }

}