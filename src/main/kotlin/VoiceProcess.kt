import java.io.BufferedReader
import java.io.InputStreamReader
import kotlin.streams.toList

class VoiceProcess: VoiceManager {
    private val path = "${System.getProperty("user.dir")}//build//python_cache//sound.py"
    private fun mainProcess(): Process{
        val p = ProcessBuilder("py", "$path").start()
        p.waitFor()
        return p
    }
    fun processor(): String {
        //.lines().toList().joinToString("\n")
        val p = mainProcess()
        val reader = BufferedReader(InputStreamReader(p.inputStream))
        return reader.lines().toList().joinToString("\n")

    }

    /**
     * destroys currently running voice process
     */
    fun stopProcess() {
        mainProcess().destroy()
    }
}