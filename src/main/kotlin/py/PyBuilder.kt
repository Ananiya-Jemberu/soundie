package py

import java.io.File
import java.io.FileWriter
import java.io.PrintWriter

class PyBuilder(val file: File) {
    val wrd = {a: String ->
        """# Import
import $a

"""
    }


    /**
     * write import script from new line (over write)
     * */
    fun import0(`package`: String){
        file.writeText(wrd(`package`).trimMargin())
    }

    /**
     * write import script from next line (update)
     *
     * usage:
     *
     * ```kt
     * fun main(){
     *    PyBuilder(file).let{
     *       it.import0("tkinter")
     *       it.import("django")
     *    }
     *
     * }
     * ```
     */
    fun import(`package`: String){
        val out = PrintWriter(FileWriter(file, true), true)
        out.append(wrd(`package`))
        out.close()
    }

    /**
     * the main body of python script
     */
    fun body(script: () -> String) {
        val out = PrintWriter(FileWriter(file, true), true)
        out.append(
            script()
        )
        out.close()
    }

}
fun main(){
    val file = File("C://Users//User//IdeaProjects//Soundie//src//main/kotlin/sound.py")
    PyBuilder(file).appUser
        import0("pyttsx3")
        import("pyttsx")
        body {
            """print(hello wordl)"""
        }
    }
}
